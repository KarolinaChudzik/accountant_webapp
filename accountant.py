from sqlite3.dbapi2 import Cursor
from typing import Tuple
from flask import Flask, render_template, request, redirect, flash
from flask.helpers import url_for
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import joinedload
import sqlite3
from flask_alembic import Alembic

app = Flask(__name__)
app.secret_key = 'test'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)
alembic = Alembic()
alembic.init_app(app)

class DataBase(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    akcja = db.Column(db.String(), nullable = False)

@app.route("/", methods=['GET', 'POST'])
def accountant():
    manager = create_manager()
    manager.read_database()
    manager.execute()

    zmiana_salda = request.form.get('value')
    komentarz = request.form.get('comment')
    purchase_index = request.form.get('purchase_index')
    purchase_price = request.form.get('purchase_price')
    purchase_qty = request.form.get('purchase_quantity')
    sales_index = request.form.get('sales_index')
    sales_price = request.form.get('sales_price')
    sales_qty = request.form.get('sales_quantity')

    if zmiana_salda and komentarz:
        log = f"Zmiana saldo: {zmiana_salda} z komentarzem: {komentarz}."
        manager.current_change(float(zmiana_salda), log)
        paramert = ["saldo, {}, {}".format(zmiana_salda, komentarz)]
        manager.append_parametry(paramert)
        manager.save_parametry()
        obj = DataBase(
            akcja=str(paramert),
            )
        db.session.add(obj)
        db.session.commit()

    if sales_index and sales_price:
        log = f"Dokonano sprzedazy produktu: {sales_index}, ilosc:" \
        f" {purchase_qty}, w cenie za szt.  {sales_price}."
        paramert = ["sprzedaz, {}, {}, {}".format(sales_index,
                                                sales_price,
                                                sales_qty)]
        manager.append_parametry(paramert)
        manager.current_changes_sales(
            sales_index,
            float(sales_price),
            float(sales_qty),
            log)
        manager.save_parametry()
        obj = DataBase(
            akcja=str(paramert),
            )
        db.session.add(obj)
        db.session.commit()

    if purchase_index and purchase_qty:
        log = f"Dokonano zakypu produktu: {purchase_index}, ilosc:" \
        f" {purchase_qty}, w cenie za szt.  {purchase_price}."
        paramert = ["zakup, {}, {}, {}".format(purchase_index,
                                            purchase_price, purchase_qty, )]
        manager.append_parametry(paramert)
        manager.current_changes_purchases(purchase_index, float(purchase_price),
                                            float(purchase_qty), log)
        manager.save_parametry()
        obj = DataBase(
            akcja=str(paramert),
            )
        db.session.add(obj)
        db.session.commit()

    my_context = {

        'saldo': manager.saldo,
        'magazyn': manager.magazyn_dict,
        'akcje': db.session.query(DataBase).all()

    }
    print(manager.parametry)

    return render_template ('homepage.html', context=my_context)

#db.create_all()


@app.route("/history/", methods=['GET', 'POST'])
@app.route("/history/<start>/<end>", methods=['GET', 'POST'])
def history_range(start=1, end=2):
    manager = create_manager()
    manager.read_database()
    manager.execute()
    
    if request.method == "POST":
        start = request.form.get("from")
        end = request.form.get("to")
        return redirect(url_for("history_range", start=start, end=end))

    my_context = {
        'from': int(start),
        'to': int(end),
        'przeglad': manager.logs,
    }
    return render_template ('history.html', context=my_context)


class Manager:

    def __init__(self):
        self.saldo = 0
        self.magazyn_dict = {}
        self.logs = []
        self.parametry = []
        self.cb = {}


    def read_database(self):
        db_products = db.session.query(DataBase).all()
        for parametr in db_products:
            print("parametr", parametr.akcja, type(parametr.akcja))
            parametry = parametr.akcja.split(',')
            parametry = [x.strip() for x in parametry]
            print("parametry", parametry, type(parametry))
            self.parametry.append(parametry)
    
    
    def read_input_data(self, file_path='input_data.txt'):
        with open(file_path) as file:
            for line in file.readlines():
                lines = line.split(",")
                lines = [x.strip() for x in lines]
                self.parametry.append(lines)

    def current_change(self, wartosc_zmiany, log):
        if wartosc_zmiany < 0 and wartosc_zmiany + self.saldo < 0:
            print("Niewystarczajace srodki!")
            flash('Niewystarczajace srodki!')
        else:
            self.saldo += wartosc_zmiany
            self.logs.append(log)

    def current_changes_purchases(self, index, price, number, log):
        wartosc_zakupu_koniec = number * price
        if wartosc_zakupu_koniec > self.saldo:
            print(f'Cena za towary ({wartosc_zakupu_koniec} '
                  f'przekracza wartosc salda {self.saldo}.)')
            flash('Cena za towary przekracza wartosc salda')
        else:
            self.saldo = self.saldo - wartosc_zakupu_koniec
            if not self.magazyn_dict.get(index):
                self.magazyn_dict[index] = {
                    "ilosc": number, "cena": price
                }
            else:
                magazyn_ilosc_prod = \
                    self.magazyn_dict[index]["ilosc"]
                self.magazyn_dict[index] = {
                    "ilosc": magazyn_ilosc_prod + number, "cena": price
                }
            self.logs.append(log)

    def current_changes_sales(self, index, price, number, log):
        wartosc_zmiany = price * number
        if not self.magazyn_dict.get(index):
            print("Brak produktu na stanie magazynowym, podaj inny produkt")
            flash('Brak produktu na stanie magazynowym, podaj inny produkt')
        else:
            if self.magazyn_dict.get(index)["ilosc"] < number:
                print("Brak wystarczajace ilosci produktow, "
                  "wprowadz inna ilosc.")
                flash('Brak wystarczajace ilosci produktow, wprowadz inna ilosc')
            magazyn_ilosc_prod = \
                self.magazyn_dict[index]["ilosc"]
            self.magazyn_dict[index] = {
                "ilosc": magazyn_ilosc_prod - number,
                "cena": price
            }
            self.saldo = self.saldo + wartosc_zmiany
            self.logs.append(log)
            if not self.magazyn_dict.get(index)['ilosc']:
                del self.magazyn_dict[index]

    def append_parametry(self, parametr):
        self.parametry.append(parametr)

    def save_parametry(self):
        with open("output_data.txt", "w") as file:
            for item in self.parametry:
                file.write(str(item) + "\n")      

    def assign(self, mode):
        def inner(func):    
            self.cb[mode] = func
        return inner

    def execute(self):
        for i in self.parametry:
            mode = i[0].replace("['", "")
            print(mode)
            if mode in self.cb:
                self.cb[mode](i[1:])
            elif mode == 'stop':
                print('Zakonczyles prace programu.')
            else:
                print(f'{mode} - nieobslugiwany rodzaj akcji.')
            


def create_manager():
    manager = Manager()

    @manager.assign(mode='saldo')
    def update_saldo(i):
        zmiana_salda = float(i[0])
        komentarz = i[1].replace("\n", "")
        log = f"Zmiana salda: {zmiana_salda} " \
                f"z komentarzem: {komentarz}."
        manager.current_change(zmiana_salda, log)

    @manager.assign(mode='zakup')
    def make_purchases(i):
            identyfikator_produktu_zakup = i[0]
            cena_jedn_zakup = int(i[1])
            ilosc_szt_zakup = int(i[2].replace("']", ""))
            log = f"Dokonano zakupu produktu {identyfikator_produktu_zakup}," \
                    f"ilosc {ilosc_szt_zakup} w cenie za szt.{cena_jedn_zakup}."
            manager.current_changes_purchases(identyfikator_produktu_zakup,
                                            cena_jedn_zakup,
                                            ilosc_szt_zakup,
                                            log)

    @manager.assign(mode='sprzedaż')
    def make_sales(i): 
            identyfikator_produktu_sprzedaz = i[0]
            cena_jedn_sprzedaz = float(i[1])
            ilosc_szt_sprzedaz = int(i[2].replace("']", ""))
            log = f"Dokonano srzedazy " \
                    f"produktu {identyfikator_produktu_sprzedaz}," \
                    f"ilosc {ilosc_szt_sprzedaz} w cenie za szt. " \
                    f"{cena_jedn_sprzedaz}."
            manager.current_changes_sales(identyfikator_produktu_sprzedaz,
                                        cena_jedn_sprzedaz,
                                        ilosc_szt_sprzedaz,
                                        log)

    return manager